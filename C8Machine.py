# This file (c) Divi 2021
# under GPL v2

from array import array
import random


class C8Machine:

    def __init__(self,
                 cls=None,
                 sprite=None,
                 keys=None,
                 buzzer=None,
                 op_time=int((1 / 1000) * 1e9)):

        self.opcode = 0  # current opcode (16 bits)
        self.i = 0  # index register (16 bits)
        self.pc = 0x200  # program counter (16 bits)
        self.sp = 0  # stack pointer (16 bits)

        self.memory = array('B', [0] * 0x1000)  # main RAM (4 KB)
        self.registers = array('B', [0] * 16)  # V registers (16 * 8 bits)
        self.font = array('B')  # font set

        self.timer_60hz = (1 / 60) * 1e9

        self.d_timer = 0  # delay timer (8 bits)
        self.s_timer = 0  # sound timer (8 bits)

        self.op_time = op_time

        self.sound_on = False

        # callbacks
        self.cb_cls = cls
        self.cb_draw_sprite = sprite
        self.keypad_status = keys
        self.buzzer = buzzer

        # main opcode selection
        self.opcode_exec = {
            0x0000: self.exec_0x0,
            0x1000: self.exec_0x1,
            0x2000: self.exec_0x2,
            0x3000: self.exec_0x3,
            0x4000: self.exec_0x4,
            0x5000: self.exec_0x5,
            0x6000: self.exec_0x6,
            0x7000: self.exec_0x7,
            0x8000: self.exec_0x8,
            0x9000: self.exec_0x9,
            0xA000: self.exec_0xa,
            0xB000: self.exec_0xb,
            0xC000: self.exec_0xc,
            0xD000: self.exec_0xd,
            0xE000: self.exec_0xe,
            0xF000: self.exec_0xf
        }

        # 0x0 opcode selection
        self.exec_0x0_options = {
            0x00E0: self.exec_0x00e0,
            0x00EE: self.exec_0x00ee
        }

        # 0x8 opcode selection
        self.exec_0x8_options = {
            0x0: self.exec_0x8xx0,
            0x1: self.exec_0x8xx1,
            0x2: self.exec_0x8xx2,
            0x3: self.exec_0x8xx3,
            0x4: self.exec_0x8xx4,
            0x5: self.exec_0x8xx5,
            0x6: self.exec_0x8xx6,
            0x7: self.exec_0x8xx7,
            0xE: self.exec_0x8xxe
        }

        # 0xE opcode selection
        self.exec_0xe_options = {
            0x9E: self.exec_0xex9e,
            0xA1: self.exec_0xexa1
        }

        # 0xF opcode selection
        self.exec_0xf_options = {
            0x07: self.exec_0xfx07,
            0x0A: self.exec_0xfx0a,
            0x15: self.exec_0xfx15,
            0x18: self.exec_0xfx18,
            0x1e: self.exec_0xfx1e,
            0x29: self.exec_0xfx29,
            0x33: self.exec_0xfx33,
            0x55: self.exec_0xfx55,
            0x65: self.exec_0xfx65
        }

        self.init_memory()

        random.seed()

    def init_memory(self):

        self.font.fromlist(
            [
                0xF0, 0x90, 0x90, 0x90, 0xF0,  # 0
                0x20, 0x60, 0x20, 0x20, 0x70,  # 1
                0xF0, 0x10, 0xF0, 0x80, 0xF0,  # 2
                0xF0, 0x10, 0xF0, 0x10, 0xF0,  # 3
                0x90, 0x90, 0xF0, 0x10, 0x10,  # 4
                0xF0, 0x80, 0xF0, 0x10, 0xF0,  # 5
                0xF0, 0x80, 0xF0, 0x90, 0xF0,  # 6
                0xF0, 0x10, 0x20, 0x40, 0x40,  # 7
                0xF0, 0x90, 0xF0, 0x90, 0xF0,  # 8
                0xF0, 0x90, 0xF0, 0x10, 0xF0,  # 9
                0xF0, 0x90, 0xF0, 0x90, 0x90,  # A
                0xE0, 0x90, 0xE0, 0x90, 0xE0,  # B
                0xF0, 0x80, 0x80, 0x80, 0xF0,  # C
                0xE0, 0x90, 0x90, 0x90, 0xE0,  # D
                0xF0, 0x80, 0xF0, 0x80, 0xF0,  # E
                0xF0, 0x80, 0xF0, 0x80, 0x80   # F
            ]
        )

        for r in range(80):
            self.memory[r] = self.font[r]

    def step(self):
        """Execute next cycle"""

        # timer update
        self.timer_60hz -= self.op_time
        if self.timer_60hz <= 0:
            self.timer_60hz = (1 / 60) * 1e9

            # delay timer update
            if self.d_timer > 0:
                self.d_timer -= 1

            # sound timer update
            if self.s_timer > 0:
                self.s_timer -= 1
                if not self.sound_on:
                    if self.buzzer is not None:
                        self.buzzer(True)
                    self.sound_on = True
            else:
                if self.sound_on:
                    if self.buzzer is not None:
                        self.buzzer(False)
                    self.sound_on = False

        # fetch next opcode
        self.opcode = self.memory[self.pc]
        self.opcode <<= 8
        self.opcode += self.memory[self.pc + 1]

        # set PC to next instruction
        self.pc += 2

        self.opcode_exec.get(self.opcode & 0xF000)()

    def exec_0x0(self):
        """cls, rts"""

        self.exec_0x0_options.get(self.opcode)()

    def exec_0x00e0(self):
        """cls
        00E0 clear the screen"""

        for r in range(0xF00, 0x1000):
            self.memory[r] = 0x00

        if self.cb_cls is not None:
            self.cb_cls()

    def exec_0x00ee(self):
        """rts
        00EE return from subroutine"""

        self.sp -= 1
        self.pc = self.memory[0xEA0 + self.sp * 2] << 8
        self.pc += self.memory[0xEA0 + self.sp * 2 + 1]

    def exec_0x1(self):
        """jmp NNN
        1NNN jump to address NNN"""

        self.pc = self.opcode & 0x0FFF

    def exec_0x2(self):
        """jsr NNN
        2NNN jump to subroutine at address NNN"""

        self.memory[0xEA0 + self.sp * 2] = (self.pc & 0x0F00) >> 8
        self.memory[0xEA0 + self.sp * 2 + 1] = self.pc & 0x00FF
        self.sp += 1
        self.pc = self.opcode & 0x0FFF

    def exec_0x3(self):
        """skeq VX, RR
        3XRR skip next instruction if register VX == constant RR"""

        if self.registers[(self.opcode & 0x0F00) >> 8] == self.opcode & 0x00FF:
            self.pc += 2

    def exec_0x4(self):
        """skne VX, RR
        4XRR skip next instruction if register VX != constant RR"""

        if self.registers[(self.opcode & 0x0F00) >> 8] != self.opcode & 0x00FF:
            self.pc += 2

    def exec_0x5(self):
        """skeq VX, VY
        5XY0 skip next instruction if register VX == register VY"""

        if self.registers[(self.opcode & 0x0F00) >> 8] == self.registers[(self.opcode & 0x00F0) >> 4]:
            self.pc += 2

    def exec_0x6(self):
        """mov VX, RR
        6XRR move constant RR to register VX"""

        self.registers[(self.opcode & 0x0F00) >> 8] = self.opcode & 0x00FF

    def exec_0x7(self):
        """add VX, RR
        7XRR add constant RR to register VX (no carry)"""

        reg = self.registers[(self.opcode & 0x0F00) >> 8] + self.opcode & 0x00FF
        if reg > 0xFF:
            reg -= 0x100
        self.registers[(self.opcode & 0x0F00) >> 8] = reg

    def exec_0x8(self):
        """mov, or, and, xor, add, sub, shr, rsb, shl"""

        vx = (self.opcode & 0x0F00) >> 8
        vy = (self.opcode & 0x00F0) >> 4

        self.exec_0x8_options.get(self.opcode & 0x000F)(vx, vy)

    def exec_0x8xx0(self, vx, vy):
        """mov VX, VY
        8XY0 move register VY into VX"""

        self.registers[vx] = self.registers[vy]

    def exec_0x8xx1(self, vx, vy):
        """or VX, VY
        8XY1 or register VY with register VX, store result into register VX"""

        self.registers[vx] |= self.registers[vy]

    def exec_0x8xx2(self, vx, vy):
        """and VX, VY
        8XY2 and register VY with register VX, store result into register VX"""

        self.registers[vx] &= self.registers[vy]

    def exec_0x8xx3(self, vx, vy):
        """xor VX, VY
        8XY3 exclusive or register VY with register VX, store result into register VX"""

        self.registers[vx] ^= self.registers[vy]

    def exec_0x8xx4(self, vx, vy):
        """add VX, VY
        8XY4 add register VY to VX, store result in register VX, carry stored in register VF"""

        r = self.registers[vx] + self.registers[vy]

        if r > 0xFF:
            r -= 0x100
            self.registers[0xF] = 1
        else:
            self.registers[0xF] = 0

        self.registers[vx] = r

    def exec_0x8xx5(self, vx, vy):
        """sub VX, VY
        8XY5 subtract register VY from VX, borrow stored in register VF"""

        r = self.registers[vx] - self.registers[vy]

        if r < 0:
            r += 0x100
            self.registers[0xF] = 1
        else:
            self.registers[0xF] = 0

        self.registers[vx] = r

    def exec_0x8xx6(self, vx, vy):
        """shr VX
        8X06 shift register VX right, bit 0 goes into register VF"""

        if self.registers[vx] % 2 == 0:
            self.registers[0xF] = 0
        else:
            self.registers[0xF] = 1

        self.registers[vx] >>= 1

    def exec_0x8xx7(self, vx, vy):
        """rsb VX, VY
        8XY7 subtract register VX from register VY, result stored in register VX, borrows goes in VF"""

        r = self.registers[vy] - self.registers[vx]

        if r < 0:
            r += 0x100
            self.registers[0xF] = 1
        else:
            self.registers[0xF] = 0

        self.registers[vx] = r

    def exec_0x8xxe(self, vx, vy):
        """shl VX
        8X0E shift register VX left, bit 7 stored into register VF"""

        if self.registers[vx] > 0x7F:
            self.registers[vx] -= 0x80
            self.registers[0xF] = 1
        else:
            self.registers[0xF] = 0

        self.registers[vx] <<= 1

    def exec_0x9(self):
        """skne VX, VY
        9XY0 skip next instruction if VX != VY"""

        if self.registers[(self.opcode & 0x0F00) >> 8] != self.registers[(self.opcode & 0x00F0) >> 4]:
            self.pc += 2

    def exec_0xa(self):
        """mvi NNN
        ANNN load index register with constant NNN"""

        self.i = self.opcode & 0x0FFF

    def exec_0xb(self):
        """jmi NNN
        BNNN jump to address NNN + register V0"""

        addr = self.opcode & 0x0FFF + self.registers[0]
        if addr > 0xFFF:
            addr -= 0xFFF
        self.pc = addr

    def exec_0xc(self):
        """rand VX, KK
        CXKK register VX = random number AND KK"""

        self.registers[(self.opcode & 0x0F00) >> 8] = random.randrange(0x100) & (self.opcode & 0x00FF)

    def exec_0xd(self):
        """sprite VX, VY, N
        DXYN draw sprite stored at address I at screen location (register VX, register VY) height N"""

        # ** screen coordinates
        sx = self.registers[(self.opcode & 0x0F00) >> 8]
        sy = self.registers[(self.opcode & 0x00F0) >> 4]

        collide = False

        sprite = array('B')

        # ** copy sprite in video RAM

        # parse each byte of the sprite
        for sprite_byte_count in range(0, self.opcode & 0x000F):

            sprite_byte_addr = self.i + sprite_byte_count

            # append the byte to sprite array for display callback
            sprite.append(self.memory[sprite_byte_addr])

            # check if byte is inside the screen vertically
            if 0 <= sy + sprite_byte_count < 32:

                # parse each bit
                for sprite_bit in range(8):
                    # if bit is set and inside screen horizontal limit
                    if self.memory[sprite_byte_addr] >> sprite_bit & 1 == 1 \
                            and 0 <= sx + 7 - sprite_bit < 64:
                        # XOR with bit currently in video memory
                        bit_pos_in_memory = 0xF00 + (sx + 7 - sprite_bit) // 8 + (sy + sprite_byte_count) * 8
                        self.memory[bit_pos_in_memory] ^= 1 << (sprite_bit - sx) % 8

                        # check collision
                        if self.memory[bit_pos_in_memory] >> (sprite_bit - sx) % 8 & 1 == 0:
                            collide = True

        # ** display sprite on screen
        if self.cb_draw_sprite is not None:
            self.cb_draw_sprite(sx, sy, sprite)

        # ** set VX if sprite collided
        if collide:
            self.registers[0xF] = 1
        else:
            self.registers[0xF] = 0

    def exec_0xe(self):
        """skpr, skup"""

        exec_func = self.exec_0xe_options.get(self.opcode & 0x00FF)
        exec_func()

    def exec_0xex9e(self):
        """skpr K
        EK9E skip if key (register rk) pressed"""

        if self.registers[(self.opcode & 0x0F00) >> 8] < len(self.keypad_status) \
                and self.keypad_status[self.registers[(self.opcode & 0x0F00) >> 8]] != 0:
            self.pc += 2

    def exec_0xexa1(self):
        """skup K
        EKA1 skip if key (register rk) not pressed"""

        if self.registers[(self.opcode & 0x0F00) >> 8] < len(self.keypad_status) \
                and self.keypad_status[self.registers[(self.opcode & 0x0F00) >> 8]] == 0:
            self.pc += 2

    def exec_0xf(self):
        """gdelay, key, sdelay, ssound, adi, font, bcd, str, ldr"""

        self.exec_0xf_options[self.opcode & 0x00FF]()

    def exec_0xfx07(self):
        """gdelay VR
        FR07 get delay timer into VR"""

        self.registers[(self.opcode & 0x0F00) >> 8] = self.d_timer

    def exec_0xfx0a(self):
        """key VR
        FR0A wait for for keypress, put key in register vr"""

        self.pc -= 2
        for k, v in self.keypad_status.items():
            if v != 0:
                self.registers[(self.opcode & 0x0F00) >> 8] = k
                self.pc += 2
                break

    def exec_0xfx15(self):
        """sdelay VR
        FR15 set the delay timer to VR"""

        self.d_timer = self.registers[(self.opcode & 0x0F00) >> 8]

    def exec_0xfx18(self):
        """ssound VR
        FR18 set the sound timer to VR"""

        self.s_timer = self.registers[(self.opcode & 0x0F00) >> 8]

    def exec_0xfx1e(self):
        """adi VR
        FR1E add register VR to the index register"""

        r = self.i + self.registers[(self.opcode & 0x0F00) >> 8]
        if r > 0xFFF:
            r -= 0x1000
        self.i = r

    def exec_0xfx29(self):
        """font VR
        FR29 point I to the sprite for hexadecimal character in VR"""

        self.i = self.registers[(self.opcode & 0x0F00) >> 8] * 5

    def exec_0xfx33(self):
        """bcd vr
        FR33 store the bcd representation of register vr at location I,I+1,I+2"""

        self.memory[self.i] = self.registers[(self.opcode & 0x0F00) >> 8] // 100
        self.memory[self.i + 1] = self.registers[(self.opcode & 0x0F00) >> 8] // 10 % 10
        self.memory[self.i + 2] = self.registers[(self.opcode & 0x0F00) >> 8] % 100 % 10

    def exec_0xfx55(self):
        """str V0-VR
        FR55 store the registers V0-VR at location I onward"""

        for r in range(((self.opcode & 0x0F00) >> 8) + 1):
            self.memory[self.i + r] = self.registers[r]

    def exec_0xfx65(self):
        """ldr V0-VR
        FR65 load registers V0-VR from location I onwards"""

        for r in range(((self.opcode & 0x0F00) >> 8) + 1):
            self.registers[r] = self.memory[self.i + r]
