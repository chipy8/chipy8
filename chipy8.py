#!/usr/bin/env python

# This file (c) Divi 2021
# under GPL v2

import sys
import time
import math
import argparse
from array import array

import C8Machine


def fatal(err):

    err_code = {
        100: "background color: bad value",
        101: "foreground color: bad value",
        200: "invalid ROM file",
        300: "missing Pygame library",
        400: "buzzer volume must be between 0 and 1"
    }

    print("* Fatal * {}.".format(err_code[err]))
    sys.exit(1)


try:
    import pygame
except ImportError:
    fatal(300)


class MainApp:
    """Main interpreter application.
        - Initialise Pygame window and display surfaces
        - Create Chip 8 machine
        - Load and launch ROM file"""

    def __init__(self, rom_file, zoom_level, bg_color, fg_color, buzzer_freq, buzzer_wave, buzzer_volume, k_layout):

        self.screen = Screen(zoom_level, bg_color, fg_color)

        self.snd_sample_rate = 44100
        self.snd_resolution = -16
        self.snd_channels = 1
        self.snd_buffer = 512

        self.buzzer_freq = buzzer_freq

        pygame.mixer.pre_init(
            frequency=self.snd_sample_rate,
            size=self.snd_resolution,
            channels=self.snd_channels,
            buffer=self.snd_buffer)

        pygame.init()
        pygame.display.set_caption("ChiPy 8")
        set_window_icon()

        self.sample = self.buzzer_sample(buzzer_wave, buzzer_volume)
        self.buzzer = pygame.mixer.Sound(self.sample)

        # timing
        self.display_refresh_delay = int((1 / 30) * 1e9)  # display refresh and events
        self.op_time = int((1 / 1500) * 1e9)  # operations

        #keyboard layouts

        self.kb_layout = k_layout

        self.key_code_qwerty = {
            pygame.K_1: 0x1,
            pygame.K_2: 0x2,
            pygame.K_3: 0x3,
            pygame.K_4: 0xC,

            pygame.K_q: 0x4,
            pygame.K_w: 0x5,
            pygame.K_e: 0x6,
            pygame.K_r: 0xD,

            pygame.K_a: 0x7,
            pygame.K_s: 0x8,
            pygame.K_d: 0x9,
            pygame.K_f: 0xE,

            pygame.K_z: 0xA,
            pygame.K_x: 0x0,
            pygame.K_c: 0xB,
            pygame.K_v: 0xF
        }

        self.key_code_azerty = {
            pygame.K_1: 0x1,
            pygame.K_2: 0x2,
            pygame.K_3: 0x3,
            pygame.K_4: 0xC,

            pygame.K_a: 0x4,
            pygame.K_z: 0x5,
            pygame.K_e: 0x6,
            pygame.K_r: 0xD,

            pygame.K_q: 0x7,
            pygame.K_s: 0x8,
            pygame.K_d: 0x9,
            pygame.K_f: 0xE,

            pygame.K_w: 0xA,
            pygame.K_x: 0x0,
            pygame.K_c: 0xB,
            pygame.K_v: 0xF
        }

        self.key_code_qwertz = {
            pygame.K_1: 0x1,
            pygame.K_2: 0x2,
            pygame.K_3: 0x3,
            pygame.K_4: 0xC,

            pygame.K_q: 0x4,
            pygame.K_w: 0x5,
            pygame.K_e: 0x6,
            pygame.K_r: 0xD,

            pygame.K_a: 0x7,
            pygame.K_s: 0x8,
            pygame.K_d: 0x9,
            pygame.K_f: 0xE,

            pygame.K_y: 0xA,
            pygame.K_x: 0x0,
            pygame.K_c: 0xB,
            pygame.K_v: 0xF
        }

        self.k_layouts = {
            "qwerty": self.key_code_qwerty,
            "azerty": self.key_code_azerty,
            "qwertz": self.key_code_qwertz
        }

        # state of each key
        self.keypad_state = {
            0x0: 0,
            0x1: 0,
            0x2: 0,
            0x3: 0,
            0x4: 0,
            0x5: 0,
            0x6: 0,
            0x7: 0,
            0x8: 0,
            0x9: 0,
            0xA: 0,
            0xB: 0,
            0xC: 0,
            0xD: 0,
            0xE: 0,
            0xF: 0
        }

        # create C8 machine object
        self.machine = C8Machine.C8Machine(
            cls=self.screen.clear_screen,
            sprite=self.screen.draw_sprite,
            keys=self.keypad_state,
            buzzer=self.buzzer_output,
            op_time=self.op_time
        )

        # open and load rom file in memory
        try:
            with open(rom_file, "rb") as program_file:
                addr = 0x200
                while byte := program_file.read(1):
                    self.machine.memory[addr] = int.from_bytes(byte, 'little')
                    addr += 1
        except OSError:
            fatal(200)

    def run(self):
        """Start the chip 8 machine"""

        # init display and operation timers
        self.disp_timer = self.op_timer = time.time_ns()

        # execution loop
        while True:

            # execute next operation
            if time.time_ns() - self.op_timer >= self.op_time:
                self.op_timer = time.time_ns()
                self.machine.step()

            # refresh display and events
            if time.time_ns() - self.disp_timer >= self.display_refresh_delay:
                self.disp_timer = time.time_ns()

                for event in pygame.event.get():

                    # QUIT event
                    if event.type == pygame.QUIT:
                        sys.exit()

                    # KEYDOWN event
                    if event.type == pygame.KEYDOWN:
                        if event.key in self.k_layouts[self.kb_layout]:
                            self.keypad_state[self.k_layouts[self.kb_layout][event.key]] = 1
                        elif event.key == pygame.K_BACKSPACE:
                            self.console_mode()

                    # KEYUP event
                    if event.type == pygame.KEYUP and event.key in self.k_layouts[self.kb_layout]:
                        self.keypad_state[self.k_layouts[self.kb_layout][event.key]] = 0

                # refresh display
                self.screen.refresh()

    def console_mode(self):
        """Enter console mode"""

        # save the chip 8 screen
        screen_copy = self.screen.pixel_surface.copy()

        # display console
        self.screen.clear_screen()

        self.screen.put_string("Emulation Paused", 0, 0, True)
        self.screen.put_string("                ", 0, 6, True)
        self.screen.put_string("[SPC] > Mem Dump", 0, 18)
        self.screen.put_string("[BKS] > Back", 0, 24)

        self.screen.refresh()

        console_mode = True

        # console execution loop
        while console_mode:
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    sys.exit()

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_BACKSPACE:
                        console_mode = False
                    if event.key == pygame.K_SPACE:
                        self.memory_dump(0x200, 0xFFF)

        # restore chip 8 screen
        self.screen.pixel_surface = screen_copy

    def memory_dump(self, start=0x000, end=0xFFF):
        """Display informations about machine state in parent shell console."""

        print('\nMemory dump ({:03X}:{:03X})'.format(start, end))
        for r in range(end + 1 - start):
            if r % 16 == 0:
                print('\n{:03X}   '.format(r + start), end='')
            print('{:02X}'.format(self.machine.memory[r + start]), end=' ')
        print('')

        print('\nRegisters')
        print('PC: {:03X}   SP: {:03X}   I: {:03X}'.format(self.machine.pc, self.machine.sp, self.machine.i))
        for r in range(0x10):
            print('V{:01X}: {:02X}   '.format(r, self.machine.registers[r]), end='')
            if r == 7:
                print('')
        print('')

        print('\nTimers')
        print('Delay: {:02X}   Sound: {:02X}'.format(self.machine.d_timer, self.machine.s_timer))

        print('\nDisplay')
        for r in range(0xF00, 0x1000):
            if r % 8 == 0:
                print('')
            for s in range(7, -1, -1):
                if self.machine.memory[r] >> s & 1 == 1:
                    print(chr(9608), end='')
                else:
                    print(' ', end='')

        print('')

    def buzzer_output(self, play_sound):
        if play_sound and not pygame.mixer.get_busy():
            self.buzzer.play(-1)
        elif not play_sound and pygame.mixer.get_busy():
            self.buzzer.stop()

    def buzzer_sample(self, buzzer_wave, vol):
        length = self.snd_sample_rate // self.buzzer_freq

        if buzzer_wave == "square":
            return square_sample(vol, length)
        elif buzzer_wave == "tri":
            return tri_sample(vol, length)
        elif buzzer_wave == "sin":
            return sin_sample(vol, length)


def square_sample(vol, length):
    smpl = array('h', [int(-0x8000 * vol)] * length)
    smpl[length // 2:] = array('h', [int(0x7FFF * vol)] * (length // 2))

    return smpl


def tri_sample(vol, length):
    smpl = array('h', [0] * length)

    a = (0x7FFF + 0x8000) * vol / (length // 2)
    b = -0x8000 * vol
    b1 = (0x7000 + 0xFFFF) * vol

    for i in range(length // 4):
        smpl[i] = int(a * (i + length // 4) + b)

    for i in range(length // 4, length // 4 * 3):
        smpl[i] = int(-a * (i + length // 4) + b1)

    for i in range(length // 4 * 3, length):
        smpl[i] = int(a * (i - length // 4 * 3) + b)

    return smpl


def sin_sample(vol, length):
    smpl = array('h', [0] * length)

    for i in range(length):
        smpl[i] = int(math.sin((i/length) * 2 * math.pi) * 0x7FFF * vol)

    return smpl


class Screen:

    def __init__(self, zoom_level, bg_color, fg_color):
        self.pixel_surf_size = self.pixel_surf_width, self.pixel_surf_height = 64, 32

        self.zoom_level = zoom_level

        self.bg_color = bg_color
        self.fg_color = fg_color

        self.bg_color_tupple = self.bg_color >> 16 & 0xFF, self.bg_color >> 8 & 0xFF, self.bg_color & 0xFF

        self.disp_surf_size = self.disp_surf_width, self.disp_surf_height = \
            self.pixel_surf_width * self.zoom_level, self.pixel_surf_height * self.zoom_level

        self.screen = pygame.display.set_mode(self.disp_surf_size)

        self.pixel_surface = pygame.Surface(self.pixel_surf_size)

        self.display_surface = pygame.Surface(self.disp_surf_size)

        self.clear_screen()

        # console font
        self.console_font = {
            'A': 0x04AEAA,
            'B': 0x0CACAC,
            'C': 0x068886,
            'D': 0x0CAAAC,
            'E': 0x0E8C8E,
            'F': 0x0E8C88,
            'G': 0x068AA6,
            'H': 0x0AAEAA,
            'I': 0x0E444E,
            'J': 0x0622A4,
            'K': 0x0AACAA,
            'L': 0x08888E,
            'M': 0x0AEAAA,
            'N': 0x0CAAAA,
            'O': 0x04AAA4,
            'P': 0x0CAC88,
            'Q': 0x04AAC6,
            'R': 0x0CACAA,
            'S': 0x06842C,
            'T': 0x0E4444,
            'U': 0x0AAAAE,
            'V': 0x0AAAA4,
            'W': 0x0AAAEA,
            'X': 0x0AA4AA,
            'Y': 0x0AA444,
            'Z': 0x0E248E,

            'a': 0x006AA6,
            'b': 0x08CAAC,
            'c': 0x006886,
            'd': 0x026AA6,
            'e': 0x004AC6,
            'f': 0x024E44,
            'g': 0x04A62C,
            'h': 0x08CAAA,
            'i': 0x040444,
            'j': 0x040448,
            'k': 0x08ACAA,
            'l': 0x044442,
            'm': 0x00EAAA,
            'n': 0x00CAAA,
            'o': 0x004AA4,
            'p': 0x0CAAC8,
            'q': 0x06AA62,
            'r': 0x006888,
            's': 0x00682C,
            't': 0x046442,
            'u': 0x00AAA6,
            'v': 0x00AAA4,
            'w': 0x00AAEA,
            'x': 0x00A4AA,
            'y': 0x00AE2E,
            'z': 0x00E28E,

            '0': 0x0EAAAE,
            '1': 0x04C44E,
            '2': 0x0E2E8E,
            '3': 0x0E262E,
            '4': 0x0AAE22,
            '5': 0x0E8E2E,
            '6': 0x0E8EAE,
            '7': 0x0E2222,
            '8': 0x0EAEAE,
            '9': 0x0EAE2E,

            ' ': 0x000000,
            '(': 0x004884,
            ')': 0x004224,
            '!': 0x044404,
            '.': 0x000004,
            ',': 0x000048,
            '\'': 0x044000,
            '[': 0x006446,
            ']': 0x006226,
            '>': 0x084248,
            '<': 0x024842,
            '?': 0x0E2604,
            '+': 0x004E40,
            '-': 0x000E00,
            '*': 0x004EA0,
            '/': 0x002480
        }

    def refresh(self):

        pygame.transform.scale(self.pixel_surface, self.disp_surf_size, self.display_surface)
        self.screen.blit(self.display_surface, (0, 0))
        pygame.display.flip()

    def clear_screen(self):

        self.pixel_surface.fill(self.bg_color_tupple)

    def draw_sprite(self, x, y, sprite):

        px_array = pygame.PixelArray(self.pixel_surface)
        for r in range(len(sprite)):
            for s in range(8):
                pix_x = s + x
                pix_y = r + y

                if pix_x > 63 or pix_x < 0 or pix_y > 31 or pix_y < 0:
                    continue

                if sprite[r] >> (7 - s) & 1:
                    if px_array[pix_x][pix_y] == self.bg_color:
                        px_array[pix_x][pix_y] = self.fg_color
                    else:
                        px_array[pix_x][pix_y] = self.bg_color
        px_array.close()

    def put_char(self, char, x, y, reverse=False):

        if not reverse:
            fg_color = self.fg_color
            bg_color = self.bg_color
        else:
            fg_color = self.bg_color
            bg_color = self.fg_color

        if char not in self.console_font:
            char = '?'

        char = self.console_font[char]

        px_array = pygame.PixelArray(self.pixel_surface)

        for r in range(6):
            char_line = char >> (4 * (5 - r))
            for s in range(4):
                if char_line >> (3 - s) & 1:
                    px_array[s + x][r + y] = fg_color
                else:
                    px_array[s + x][r + y] = bg_color

        px_array.close()

    def put_string(self, string, x, y, reverse=False):

        for r in range(len(string)):
            self.put_char(string[r], x + 4 * r, y, reverse)


def set_window_icon(size=256):

    icon = [
        0b0000010001000000000,
        0b0110010000011100000,
        0b1001011001010010101,
        0b1000010101011100101,
        0b1001010101010000011,
        0b0110010101010000001,
        0b0000000000000000001,
        0b0111111111111111110,
        0b0000000000000000000,
        0b0000000000001111100,
        0b0000000000011111110,
        0b0000000000011000110,
        0b0000000000011000110,
        0b0000000000001111100,
        0b0000000000011111110,
        0b0000000000011000110,
        0b0000000000011000110,
        0b0000000000011111110,
        0b0000000000001111100
    ]

    pix_surface = pygame.Surface((19, 19))

    px_array = pygame.PixelArray(pix_surface)

    for line in range(len(icon)):
        for bit in range(0, 19):
            if icon[line] >> bit & 1 == 1:
                px_array[18 - bit][line] = 0xFFFFFF

    px_array.close()

    bitmap_surface = pygame.Surface(((size // 19) * 19, (size // 19) * 19))
    pygame.transform.scale(pix_surface, ((size // 19) * 19, (size // 19) * 19), bitmap_surface)

    icon_surface = pygame.Surface((size, size))
    icon_surface.blit(bitmap_surface, (
            (size - (size // 19) * 19) / 2,
            (size - (size // 19) * 19) / 2,
            size // 19,
            size // 19))

    pygame.display.set_icon(icon_surface)


if __name__ == "__main__":

    # welcome message
    print("\nChipy8 by Divi 2021 - a Chip-8 interpreter written in Python.")
    print("Distributed under GPL v2 license.\n")

    # argument handling
    argParser = argparse.ArgumentParser()
    argParser.add_argument("ROM_file", help="ROM file to be executed by chipy8.")
    argParser.add_argument("-z", type=int, default=10, help="Zoom level (default 10).")
    argParser.add_argument("-b", default="000000", help="Background color (hex RRGGBB, default=000000).")
    argParser.add_argument("-f", default="F0F0F0", help="Foreground color (hex RRGGBB, default=F0F0F0).")
    argParser.add_argument("-n", type=int, default=220, help="Buzzer frequency (Hz, default=220).")
    argParser.add_argument("-w", default="square", choices=["square", "tri", "sin"],
                           help="Buzzer waveform (default=square).")
    argParser.add_argument("-v", type=float, default=0.2, help="Buzzer volume (min=0, max=1, default=0.2).")
    argParser.add_argument("-k", default="qwerty", choices=["qwerty", "azerty", "qwertz"],
                           help="Select keyboard layout (default=qwerty).")
    args = argParser.parse_args()

    # check backround and foreground color arguments
    back_color = fore_color = 0
    try:
        back_color = int(args.b, base=16)
    except ValueError:
        fatal(100)

    try:
        fore_color = int(args.f, base=16)
    except ValueError:
        fatal(101)

    if args.v < 0 or args.v > 1:
        fatal(400)

    app = MainApp(args.ROM_file, args.z, back_color, fore_color, args.n, args.w, args.v, args.k)
    app.run()
