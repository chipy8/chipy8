# Chipy8
## A Chip-8 interpreter
### Usage
chipy8.py [-h] [-z Z] [-b B] [-f F] [-n N] [-w {square,tri,sin}] [-v V] [-k {qwerty,azerty,qwertz}] ROM_file  
  
ROM_file: ROM file to be executed by chipy8.  
  
optional arguments:  
-h, --help: show this help message and exit.  
-z: Zoom level (default 10).  
-b: Background color (hex RRGGBB, default=000000).  
-f: Foreground color (hex RRGGBB, default=F0F0F0).  
-n: Buzzer frequency (default=220).  
-w{square,tri,sin}: Buzzer waveform (default=square).  
-v: Buzzer volume (min=0, max=1, default=0.2).  
-k {qwerty,azerty,qwertz}: Select keyboard layout (default=qwerty).  
  
### Requires  
Python 3.x and Pygame 2.x  
  
### Key bindings  
Default keyboard layout is QWERTY.  
You can adjust that with the -k command line argument.  

Chip-8 has a 16 keys keyboard with this layout:  

+---+---+---+---+  
| 1 | 2 | 3 | C |  
+---+---+---+---+  
| 4 | 5 | 6 | D |  
+---+---+---+---+  
| 7 | 8 | 9 | E |  
+---+---+---+---+  
| A | 0 | B | F |  
+---+---+---+---+  

which is mapped like this on a QWERTY modern keyboard:

+---+---+---+---+  
| 1 | 2 | 3 | 4 |  
+---+---+---+---+  
| Q | W | E | R |  
+---+---+---+---+  
| A | S | D | F |  
+---+---+---+---+  
| Z | X | C | V |  
+---+---+---+---+  
